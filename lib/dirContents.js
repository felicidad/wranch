const Path = require('path');
const Fs = require('fs');

/**
 * Reads contents from all files in `dirname` and maps as key: value pairs
 *
 * @kind function
 * @name dirContents
 *
 * @param {string} dirname - Path to require plugin from
 * @param {object} options - Options to pass
 *
 * @example
 *
 * const contents = dirContents('./test/fixture');
 * // {
 * //    'extensions/js': 'module.exports = \'js\';\n',
 * //    'extensions/json': '["json"]',
 * //    'flat/index':
 * //     'module.exports = [{ name: \'flat\' }, 1, 2, 3, \'a\', \'b\', \'c\'];\n',
 * //    'fldr/fldrer/fldrerest/goal': '<h1>success!</h1>',
 * //    'fldr/index': 'module.exports = { name: \'fldr\' };\n',
 * //    ignore: '',
 * //    index: '',
 * //    'methods/dontwork': 'module.exports = () => \'not work!\';\n\n',
 * //    'methods/index': 'module.exports = false;\n',
 * //    'methods/notFunction': 'module.exports = \'not a function\';',
 * //    'methods/work': 'module.exports = () => \'works!\';\n\n',
 * //    'noindex/test': 'module.exports = true;\n',
 * //    tast: 'module.exports = { name: \'tast\' };\n',
 * //    test: 'module.exports = { name: \'test\' };\n',
 * //    tist: 'module.exports = { name: \'tist\' };\n'
 * // }
 *
 * // And so you can do things like this:
 * Object.entries(contents).forEach(([name, content]) => {
 *
 *      Handlebars.registerPartial(name, content);
 * });
 *
 * // You can also pass the following options:
 * const contents = dirContents('./test/fixture', {
 *      // Delimiter to separate folder hierarchy
 *      delimiter: '-',
 *
 *      // Name of folder being recursively read
 *      parentName: 'test',
 *
 *      // Parent content object to attach objects recursively
 *      parentContent: {}
 * });
 *
 * @return {object} An object of `{ [filename]: fileContents }`
 */
const dirContents = (dirname, options = {}) => {

    const { delimiter = '/', parentName, contents = {} } = options;

    Fs.readdirSync(dirname)
        .forEach((file) => {

            const filename = file.split('.').shift();
            const filePath = Path.resolve(dirname, file);
            const stat = Fs.statSync(filePath);


            let keyName = filename;

            if (parentName) {

                keyName = [parentName, filename].join(delimiter);
            }

            if (stat.isFile()) {

                const content = Fs.readFileSync(filePath).toString();

                contents[keyName] = content;
            }

            if (stat.isDirectory()) {

                dirContents(filePath, { delimiter, parentName: keyName, contents });
            }


        });

    return contents;
};

module.exports = dirContents;
