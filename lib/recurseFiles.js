const Fs = require('fs');
const Hoek = require('@hapi/hoek');

/**
 * Performs file operations recursively
 *
 * @name recurseFiles
 * @kind function
 *
 * @param {string} path - required - Path to start crawling
 * @param {function} fileFn - required - Function to execute on a file
 * @param {function} dirFn - optional - Callback passes original path variable
 *
 * @example
 *
 * // Example 1
 * watchFile = (file) => fs.watch(file, () => {
 *
 *     socket.send('filechange', file);
 * });
 *
 * recurseFile('./lib', watchFile);
 *
 *
 * // Example 2 (deleteRecursively)
 *
 * const rm = (file) => Fs.unlinkSync(file);
 * const rmRf = (dir) => Fs.rmdirSync(dir);
 *
 * const deleteRecursively = (path) => RecurseFiles(path, rm, rmRf);
 *
 */
const crawl = (path, fileFn, dirFn) => {

    Hoek.assert(typeof path === 'string', 'path must be a string');
    Hoek.assert(typeof fileFn === 'function', 'fileFn must be a function');

    if (dirFn) {

        Hoek.assert(typeof dirFn === 'function', 'cb must be a function');
    }

    if (Fs.existsSync(path)) {

        Fs.readdirSync(path).forEach((file) => {

            const curPath = path + '/' + file;

            // recurse
            if (Fs.statSync(curPath).isDirectory()) {

                crawl(curPath, fileFn, dirFn);
            }
            else {

                fileFn(curPath);
            }
        });

        dirFn && dirFn(path);
    }
};

module.exports = crawl;
