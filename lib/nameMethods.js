const MapFiles = require('./mapFiles');

/**
 * Uses `mapFiles` to imports file and converts them to an array of objects with key as `name` and export as `method`. Will only convert where export is a function.
 * @name nameMethods
 * @kind function
 * @param {string} dirname - Directory to get items from
 *
 * @example
 * const methods = nameMethod('./lib');
 * // [
 * //   { name: 'bindFunctions', method: [Function: bindFunctions] },
 * //   { name: 'deleteRecursive', method: [Function] },
 * //   { name: 'dirContents', method: [Function: dirContents] },
 * //   { name: 'listModules', method: [Function] },
 * //   { name: 'mapArray', method: [Function] },
 * //   { name: 'mapFiles', method: [Function] },
 * //   { name: 'nameMethods', method: [Function] },
 * //   { name: 'noop', method: [Function] },
 * //   { name: 'recurseFiles', method: [Function: crawl] },
 * //   { name: 'requireFiles', method: [Function] },
 * //   { name: 'testMirror', method: [Function] },
 * //   { name: 'testMirrorPath', method: [Function] },
 * //   { name: 'tryRequirePlugins', method: [Function] }
 * // ]
 *
 * server.methods(methods);
 *
 * @return {array} An array of `[{ name, method }]`
 */
module.exports = (dirname) =>

    Array.from(
        MapFiles(dirname).entries()
    ).filter(
        (entry) => typeof entry[1] === 'function'
    ).map(
        ([name, method]) => ({ name, method })
    );
