const Fs = require('fs');

/**
 * Lists all files in a path except `index.js`
 *
 * @name listModules
 * @kind function
 *
 * @param {string} dirname - Path to list files
 *
 * @example
 * listModules('./test/fixtures')
 * // [
 * //      'extensions',
 * //      'flat',
 * //      'fldr',
 * //      'ignore.php',
 * //      'ignore.py',
 * //      'ignore.rb',
 * //      'methods',
 * //      'noindex',
 * //      'tast.js',
 * //      'test.js',
 * //      'tist.js'
 * // ]
 *
 * @return {array} An array of paths except `index.js`
 */
module.exports = (dirname) =>

    Fs.readdirSync(dirname)
        .filter((f) => f !== 'index.js');
