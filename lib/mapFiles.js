const Path = require('path');
const Fs = require('fs');
const Ls = require('./listModules');

const fileEntry = (dir, file) => [
    file.split('.').shift(),
    require(Path.resolve(dir, file))
];

/**
 * Uses `listModules` and requires all js modules in give `dir` and place them into a JS map
 *
 * @name mapFiles
 * @kind function
 *
 * @param {string} dirname - Path to require plugin from
 *
 * @example
 * mapFiles('./lib')
 * // Map {
 * //     'bindFunctions' => [Function: bindFunctions],
 * //     'deleteRecursive' => [Function],
 * //     'dirContents' => [Function: dirContents],
 * //     'listModules' => [Function],
 * //     'mapArray' => [Function],
 * //     'mapFiles' => [Function],
 * //     'nameMethods' => [Function],
 * //     'noop' => [Function],
 * //     'recurseFiles' => [Function: crawl],
 * //     'requireFiles' => [Function],
 * //     'testMirror' => [Function],
 * //     'testMirrorPath' => [Function],
 * //     'tryRequirePlugins' => [Function]
 * // }
 *
 * @return {map} a js map of files using their name minus extension as the key, and the module export as the value
 */
module.exports = function (dirname) {

    const modules = Ls(dirname).map((file) => {

        const stat = Fs.statSync(Path.resolve(dirname, file));

        if (stat.isFile() && /\.(js|json)/.test(file)) {

            return fileEntry(dirname, file);
        }

        if (stat.isDirectory()) {

            const files = Fs.readdirSync(Path.resolve(dirname, file));
            if (files.includes('index.js')) {

                return fileEntry(dirname, file);
            }
        }
    })
        .filter((file) => !!file);

    return new Map(modules);
};
