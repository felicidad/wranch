const Hoek = require('@hapi/hoek');

const MapFiles = require('./mapFiles');

/**
 * Requires all modules in `dirname` and flattens them
 *
 * @name mapArray
 * @kind function
 *
 * @param {string} dirname - Path to require plugin from
 *
 * @example
 * mapArray('./lib');
 *
 * // [
 * //      [Function: bindFunctions],
 * //      [Function],
 * //      [Function: dirContents],
 * //      [Function],
 * //      [Function],
 * //      [Function],
 * //      [Function],
 * //      [Function],
 * //      [Function: crawl],
 * //      [Function],
 * //      [Function],
 * //      [Function],
 * //      [Function]
 * // ]
 *
 * @return {array} An array of required modules
 */
module.exports = (dirname) =>

    Hoek.flatten(
        Array.from(
            MapFiles(dirname)
                .values()
        )
    );
