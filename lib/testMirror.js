const TestMirrorPath = require('./testMirrorPath');

/**
 * Requires a module from root folder mirrored in /test. Allows for renaming and moving of module / test files without breaking test as long as hierarchy stays the same.
 *
 * @name testMirror
 * @kind function
 *
 * @param {string} pathname - Path to require plugin from
 * @param {string} replace - Path to replace, in case your path is not '/test'. Optional
 *
 * @example
 *
 * // test/lib/utils/myModule.js will require lib/utils/myModule.js
 * const ModuleToTest = testMirror(__filename);
 *
 * // test/server/lib/utils/myModule.js will require lib/utils/myModule.js
 * const ModuleToTest = testMirror(__filename, 'test/server');
 *
 * @return {module} The mirrored module
 */

module.exports = (...args) =>

    require(
        TestMirrorPath(...args)
    );
