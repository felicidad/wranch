const MapFiles = require('./mapFiles');

const reducer = (acc, [name, mod]) => Object.assign(acc, { [name]: mod });

/**
 * Requires all js modules in `dirname` and maps as Hapi methods array
 *
 * @name requireFiles
 * @kind function
 *
 * @param {string} dirname - Path to require plugin from
 *
 * @example
 *
 * const { listModules } = requireFiles('./lib');
 *
 * @return {object} An object with in the format of `{ key: module }`
 */
module.exports = (dirname) =>

    Array.from(
        MapFiles(dirname)
            .entries()
    )
        .reduce(reducer, {});
