const Path = require('path');

/**
 * Creates an absolute path from given `pathname` that replaces `/test` by default.
 *
 * @name testMirrorPath
 * @kind function
 *
 * @param {string} pathname - Path to require plugin from
 * @param {string} replace - Path to replace, in case your path is not '/test'. Optional
 *
 * @example
 *
 * // test/lib/utils/myModule.js
 * const ModuleToTest = testMirrorPath(__filename);
 * // /home/me/myproject/lib/utils/myModule.js
 *
 * // test/server/lib/utils/myModule.js
 * const ModuleToTest = testMirrorPath(__filename, 'test/server');
 * // /home/me/myproject/lib/utils/myModule.js
 *
 * @return {string} An absolute path to your mirrored module
 */

module.exports = (pathname, replace = 'test') =>

    Path.resolve(pathname).replace(`/${replace}`, '');
