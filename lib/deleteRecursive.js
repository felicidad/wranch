const Fs = require('fs');
const RecurseFiles = require('./recurseFiles');

const rm = (file) => Fs.unlinkSync(file);
const rmRf = (dir) => Fs.rmdirSync(dir);

/**
 * Deletes files recursively
 *
 * @name deleteRecursive
 * @kind function
 *
 * @param {string} dirname Path to start deleting from
 *
 * @example
 *
 * // Make a temporary folder
 * const tmpfolder = __dirname + '/tmp';
 *
 * try {
 *     Fs.mkdirSync(tmpfolder + '');
 * }
 * catch (e) {}
 *
 * // Make folders and files inside of tmpfolder
 * ['test', 'tast', 'tist'].forEach((name) => {
 *
 *     hierarchy = [hierarchy, name].join('/');
 *     try {
 *
 *         Fs.mkdirSync(tmpfolder + hierarchy);
 *     }
 *     catch (e) {}
 *
 *     try {
 *         const file = Fs.openSync(tmpfolder + hierarchy + '.txt', 'w');
 *
 *         Fs.closeSync(file);
 *     }
 *     catch (e) {}
 * });
 *
 * // Delete the entire folder recursively
 * DeleteRecursive(tmpfolder);
 */
module.exports = (dirname) => RecurseFiles(dirname, rm, rmRf);
