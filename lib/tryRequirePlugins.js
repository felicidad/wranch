const Hoek = require('@hapi/hoek');
const Joi = require('@hapi/joi');

const OptionsSchema = Joi.array().items(
    Joi.object().keys({
        plugin: Joi.string(),
        options: Joi.object().optional()
    })
);

/**
 * Attempts to require a Hapi plugin
 *
 * @name tryRequirePlugins
 * @kind function
 *
 * @param {array} plugins - Array of Hapi plugin objects without `require`ing the module
 * @return {array} An array of Hapi plugins with module required
 *
 * @example
 *
 * const plugins = TryRequirePlugins([
 *     { plugin: '@hapi/code', options: {} }, // exists
 *     { plugin: 'blipp', options: {} }, // does not exist
 *     { plugin: 'confidence', options: {} } // exists
 * ]);
 *
 * @returns An array of hapi plugins
 */

module.exports = (plugins) => {

    const valid = OptionsSchema.validate(plugins);
    Hoek.assert(valid.error === null, valid.error);

    return plugins.map(({ plugin, options }) => {

        try {

            return {
                options,
                plugin: require(plugin)
            };
        }
        catch (e) {

            console.log('\n', e.message);
            return false;
        }
    })
        .filter((p) => !!p);
};
