/**
 * Iterates through an object and binds functions. Returns a new object.
 *
 * @name bindFunctions
 * @kind function
 *
 * @param {object} obj Object to iterate over
 * @param {object} bindTo Object to bind to
 *
 * @example
 * const obj1 = {
 *      fn1: function () { console.log(this.a) }
 *      fn2: function () { console.log(this.b) }
 * }
 *
 * const bindTo = {
 *      a: true,
 *      b: false
 * }
 *
 * bindFunctions(obj1, bindTo);
 *
 * @return {object} A new object with bound functions
 */
const bindFunctions = (obj, bindTo) =>

    Object.entries(obj)
        .map(([key, val]) => {

            if (typeof val === 'function') {

                val = val.bind(bindTo);
            }

            return { [key]: val };
        })
        .reduce(
            (acc, cur) =>

                Object.assign(acc, cur)
            , obj);

module.exports = bindFunctions;
