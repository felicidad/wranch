// NPM Modules
const Lab  = require('@hapi/lab');
const Code = require('@hapi/code');

// App Modules
const BindFunction = require('../../lib/bindFunctions');

// Module Variables
const lab = exports.lab = Lab.script();
const expect = Code.expect;

lab.experiment('BindFunction',  () => {

    lab.test('it should be a function', () => {

        expect(BindFunction).to.be.a.function();
    });

    lab.test('it should bind functions in an object to another object', () => {

        const bindTo = {
            a: true,
            b: false
        };

        const obj = {
            test: function () {

                expect(this.a).to.be.true();
            },
            tast: function () {

                expect(this.b).to.be.false();
            },
            c: 'should retain original value'
        };

        const newObj = BindFunction(obj, bindTo);

        newObj.test();
        newObj.tast();
        expect(newObj.c).to.equal(obj.c);
    });

});
