// NPM Modules
const Lab  = require('@hapi/lab');
const Code = require('@hapi/code');

// App Modules
const NameMethods = require('../../lib/nameMethods');

// Module Variables
const lab = exports.lab = Lab.script();
const expect = Code.expect;

const mapNames = (arr) => arr.map(({ name }) => name);
const mapMethods = (arr) => arr.map(({ method }) => method);

lab.experiment('NameMethods',  () => {

    lab.test('it should be a function', () => {

        expect(NameMethods).to.be.a.function();
    });

    lab.test('it should require files as array of name methods', () => {

        const arr = NameMethods(__dirname + '/../fixture/methods');

        const mappedNames = mapNames(arr);

        expect(mappedNames).to.include(['work', 'dontwork']);
    });

    lab.test('it should NOT require index or files that do not return methods', () => {

        const arr = NameMethods(__dirname + '/../fixture/methods');

        const mappedNames = mapNames(arr);
        const mappedMethods = mapMethods(arr);

        expect(mappedNames).to.not.include(['notFunction', 'index']);

        const expectedOutput = [
            'works!',
            'not work!'
        ];

        expect(expectedOutput).to.include(mappedMethods[0]());
        expect(expectedOutput).to.include(mappedMethods[1]());
    });

});
