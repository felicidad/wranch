// NPM Modules
const Lab  = require('@hapi/lab');
const Code = require('@hapi/code');

// App Modules
const RecurseFiles = require('../../lib/recurseFiles');

// Module Variables
const lab = exports.lab = Lab.script();
const expect = Code.expect;


lab.experiment('RecurseFiles',  () => {

    lab.test('it should be a function', () => {

        expect(RecurseFiles).to.be.a.function();
    });

    lab.test('it should iterate through a directory and perform function on files', () => {

        const files = [];
        const folders = [];
        RecurseFiles(
            __dirname,
            (f) => files.push(f.split('wranch/').pop()),
            (d) => folders.push(d.split('wranch/').pop())
        );

        const expectedFiles = [
            'test/lib/deleteRecursive.js',
            'test/lib/dirContents.js',
            'test/lib/listModules.js',
            'test/lib/mapArray.js',
            'test/lib/mapFiles.js',
            'test/lib/recurseFiles.js',
            'test/lib/requireFiles.js',
            'test/lib/testMirror.js',
            'test/lib/testMirrorPath.js',
            'test/lib/tryRequirePlugins.js'
        ];

        const expectedDirs = ['test/lib'];

        expect(files).to.include(expectedFiles);
        expect(folders).to.include(expectedDirs);
    });


    lab.test('it should work without a callback', () => {


        expect(() =>

            RecurseFiles(
                __dirname,
                () => {}
            )
        ).to.not.throw();

    });
});
