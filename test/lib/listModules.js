// NPM Modules
const Lab  = require('@hapi/lab');
const Code = require('@hapi/code');

// App Modules
const ListModules = require('../../lib/listModules');

// Module Variables
const lab = exports.lab = Lab.script();
const expect = Code.expect;


lab.experiment('listModules',  () => {

    lab.test('it should be a function', () => {

        expect(ListModules).to.be.a.function();
    });

    lab.test('it list folders and js files only without index.js', () => {

        const items = ListModules(__dirname + '/../fixture');

        expect(items).to.include(['tast.js', 'test.js', 'tist.js', 'fldr']);
        expect(items).to.not.include(['index.js', 'ignore.php', 'ignore.py', 'ignore.rb']);
    });
});
