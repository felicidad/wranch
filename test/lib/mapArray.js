// NPM Modules
const Lab  = require('@hapi/lab');
const Code = require('@hapi/code');

// App Modules
const MapArray = require('../../lib/mapArray');

// Module Variables
const lab = exports.lab = Lab.script();
const expect = Code.expect;


lab.experiment('MapArray',  () => {

    lab.test('it should be a function', () => {

        expect(MapArray).to.be.a.function();
    });

    lab.test('it requires files and flattens them into an array', () => {

        const items = MapArray(__dirname + '/../fixture');

        expect(items).to.include([1, 2, 3, 'a', 'b', 'c']);
        expect(items).to.include([
            { name: 'flat' },
            { name: 'fldr' },
            { name: 'test' },
            { name: 'tist' },
            { name: 'tast' }
        ]);
    });
});
