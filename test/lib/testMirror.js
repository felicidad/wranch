// NPM Modules
const Lab  = require('@hapi/lab');
const Code = require('@hapi/code');

// App Modules
const TestMirror = require('../../lib/testMirror');

// Module Variables
const lab = exports.lab = Lab.script();
const expect = Code.expect;


lab.experiment('TestMirror',  () => {

    lab.test('it should be a function', () => {

        expect(TestMirror).to.be.a.function();
    });

    lab.test('it should require the file or directory without the test path', () => {

        const mirror = TestMirror(__filename);
        expect(mirror).to.be.a.function();
        expect(mirror).to.equal(TestMirror);
    });
});
