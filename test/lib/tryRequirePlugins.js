// NPM Modules
const Lab  = require('@hapi/lab');
const Code = require('@hapi/code');
const Confidence = require('confidence');

// App Modules
const TryRequirePlugins = require('../../lib/tryRequirePlugins');

// Module Variables
const lab = exports.lab = Lab.script();
const expect = Code.expect;


lab.experiment('TryRequirePlugins',  () => {

    lab.test('it should be a function', () => {

        expect(TryRequirePlugins).to.be.a.function();
    });

    lab.test('it should not allow bad payloads', () => {

        expect(() => TryRequirePlugins({})).to.throw('"value" must be an array');
        expect(() => TryRequirePlugins({ plugin: () => {} })).to.throw('"value" must be an array');
    });

    lab.test('it should attempt to require hapi plugins even if not exist', () => {

        const plugins = TryRequirePlugins([
            { plugin: '@hapi/code', options: {} },
            { plugin: 'blipp', options: {} },
            { plugin: 'confidence', options: {} }
        ]);

        expect(plugins.length).to.equal(2);
        expect(plugins[0].plugin).to.equal(Code);
        expect(plugins[1].plugin).to.equal(Confidence);
    });
});
