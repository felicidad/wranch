// NPM Modules
const Lab  = require('@hapi/lab');
const Code = require('@hapi/code');

// App Modules
const MapFiles = require('../../lib/mapFiles');

// Module Variables
const lab = exports.lab = Lab.script();
const expect = Code.expect;


lab.experiment('MapFiles',  () => {

    lab.test('it should be a function', () => {

        expect(MapFiles).to.be.a.function();
    });

    lab.test('it requires files into js map', () => {

        const items = MapFiles(__dirname + '/../fixture');

        expect(items).to.be.an.instanceof(Map);

        expect(items.get('flat')).to.include({ name: 'flat' });
        expect(items.get('fldr')).to.equal({ name: 'fldr' });
        expect(items.get('tast')).to.equal({ name: 'tast' });
        expect(items.get('test')).to.equal({ name: 'test' });
        expect(items.get('tist')).to.equal({ name: 'tist' });
    });

    lab.test('it ignores folders without index', () => {

        const items = MapFiles(__dirname + '/../fixture');

        expect(items).to.be.an.instanceof(Map);

        expect(items.get('noindex')).to.not.exist();
    });

    lab.test('it should import json and node extensions', () => {

        const items = MapFiles(__dirname + '/../fixture/extensions');

        expect(items).to.be.an.instanceof(Map);

        expect(items.get('json')).to.include(['json']);
        expect(items.get('js')).to.equal('js');
    });
});
