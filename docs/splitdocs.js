/* eslint-disable */
const path = require('path');
const fs = require('fs');

const deleteRecursive = require('../lib/deleteRecursive');

const p = path.resolve(__dirname, '..', 'output');

const readme = fs.readdirSync(p)
    .map(
        (filename) =>

            fs.readFileSync(path.resolve(p, filename)).toString()
    )
    .map(
        (content) =>

            content
                // .split('Global')[1]
                .split('* * *')[1]
                .trim()
    )
    .join('\n\n\n---\n\n\n');

const head = fs.readFileSync(path.resolve(__dirname, 'head.md')).toString();
fs.writeFileSync(path.resolve(__dirname, '..', 'readme.md'), [head, readme].join(' '));

deleteRecursive(p);

/* eslint-enable */
