# Hapi filesystem utilities

Filesystem tools for building `hapi` apps.

![tests](https://gitlab.com/felicidad/wranch/badges/master/pipeline.svg?style=flat)
![coverage](https://gitlab.com/felicidad/wranch/badges/master/coverage.svg?style=flat)


```
npm i -s wranch
```

- [bindFunctions](#bindfunctionsobj-bindto)
- [deleteRecursive](#deleterecursivedirname)
- [dirContents](#dircontentsdirname-options)
- [listModules](#listmodulesdirname)
- [mapArray](#maparraydirname)
- [mapFiles](#mapfilesdirname)
- [nameMethods](#namemethodsdirname)
- [recurseFiles](#recursefilespath-filefn-dirfn)
- [requireFiles](#requirefilesdirname)
- [testMirror](#testmirrorpathname-replace)
- [testMirrorPath](#testmirrorpathpathname-replace)
- [tryRequirePlugins](#tryrequirepluginsplugins)

### Examples

All examples are based on the following trees:

##### `./lib`
```
lib
├── bindFunctions.js
├── deleteRecursive.js
├── dirContents.js
├── listModules.js
├── mapArray.js
├── mapFiles.js
├── nameMethods.js
├── noop.js
├── recurseFiles.js
├── requireFiles.js
├── testMirror.js
├── testMirrorPath.js
└── tryRequirePlugins.js
```

##### `./test/fixture`
```
test/fixture
├── extensions
│   ├── js.js
│   └── json.json
├── flat
│   └── index.js
├── fldr
│   ├── fldrer
│   │   └── fldrerest
│   │       └── goal.html
│   └── index.js
├── ignore.php
├── ignore.py
├── ignore.rb
├── index.js
├── methods
│   ├── dontwork.js
│   ├── index.js
│   ├── notFunction.js
│   └── work.js
├── noindex
│   └── test.js
├── tast.js
├── test.js
└── tist.js
```

# API



